Ansible Role: Bird RS (for IXP Manager)
=========

An ansible role to install a bird (ver. 2.x) route-server for a running IXP Manager installation.

Requirements
------------

 1. A running and configured IXP Manager installation.

 2. An Ubunutu 18.04 Server/Vm/Instance.

Role Variables
--------------

```
  vars:
    ixp_mgr_fqdn: "mgr.example-ixp.net"
    ixp_mgr_api_key: "very_secret_key_data"
    ixp_mgr_handles_list:
      - "rs1-ixlan-ipv4"
      - "rs1-ixlan-ipv6"
```

License
-------

Apache 2.0
